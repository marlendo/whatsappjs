const fs = require("fs");
const path = require('path');
const express = require("express");
const serveIndex = require('serve-index')
const morgan = require("morgan");
const helmet = require("helmet");
const cors = require("cors");
const venom = require("venom-bot");

require("dotenv").config();

const middlewares = require("./middlewares");
const api = require("./api");

const app = express();
const venomSessionName = "seedsSession";

app.use(express.static('public'));
app.use(morgan("dev"));
app.use(helmet());
app.use(cors());
app.use(express.json());

global.client = 'deploy';

// venom
//   .create(
//     venomSessionName,
//     (_base64Qrimg, asciiQR, attempts, urlCode) => {
//       console.log('Number of attempts to read the qrcode: ', attempts);
//       console.log('Terminal qrcode: ', asciiQR);
//       // console.log('base64 image string qrcode: ', base64Qrimg);
//       console.log('urlCode (data-ref): ', urlCode);
//       // var matches = _base64Qrimg.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
//       //   response = {};

//       // if (matches.length !== 3) {
//       //   return new Error("Invalid input string");
//       // }
//       // response.type = matches[1];
//       // response.data = new Buffer.from(matches[2], "base64");

//       // var imageBuffer = response;
//       // fs.writeFile(
//       //   "public/qr.png",
//       //   imageBuffer["data"],
//       //   "binary",
//       //   function (err) {
//       //     if (err != null) {
//       //       console.log(err);
//       //     }
//       //   }
//       // );
//     },
//     undefined,
//     { logQR: false }
//   )
//   .then((client) => {
//     global.client = client;
//   })
//   .catch((error) => console.log(error));

//   global.client.onStateChange((state) => {
//     if ('OPENING'.includes(state)) {
//           // remove the token
//           token_path = `tokens/${session_name}.data.json`
//           console.log('unlinking ', token_path)
//           fs.unlink(`tokens/${session_name}.data.json`, (err => console.log(err)))
//           console.log('REOPENING');
//           // reinitiate venom
//           init_venom(session_name, hook, handle_function);
//         }
// }

app.get("/", (_req, res) => {
  res.setHeader("Content-Type", "text/html");
    res.send(`
    <h1>SCAN HERE</h1>
    <img src="../qr.png" alt="Girl in a jacket" width="500" height="500">
    `);
});


app.use("/api/v1", api);

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;
