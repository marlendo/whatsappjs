function notFound(req, res, next) {
  res.status(404);
  const error = new Error(`🔍 - Not Found - ${req.originalUrl}`);
  next(error);
}

function errorHandler(err, _req, res, _next) {
  const statusCode = res.statusCode !== 200 ? res.statusCode : 500;
  res.status(statusCode);
  res.json({
    success: false,
    message: err.message,
    data: null
    // stack: process.env.NODE_ENV === 'production' ? '🥞' : err.stack
  });
}

function auth(req, res, next) {
  const { authorization = "" } = req.headers;
  const [type, token] = authorization.split(" ");
  let success = false;
  let message = 'token required';

  if (type && token){
    if (type === 'Bearer'){
      try {
        let getToken = Buffer.from(token, 'base64').toString();
        if (getToken && getToken === 'MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBANDjylz9hZ18jD4KjCVtwNEYgTN6ufA+B2wwUj3z31JrSRcRWzEkYK6kmk/K+S3Qgb8uC0LI8Txx3jym3cYILH8CAwEAAQ=='){
          next();
        }
      } catch (_error) {
        res.json({
          success,
          message: 'Error decode token',
          data: null
        });
      }
    } else {
      res.json({
        success,
        message: 'Wrong token format',
        data: null
      });
    }
  } else {
    res.json({
      success,
      message,
      data: null
    });
  }
}

module.exports = {
  notFound,
  errorHandler,
  auth
};
