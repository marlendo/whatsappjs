const express = require("express");

const imageScraperCtrl = require("./imageScraper");

const router = express.Router();
const { auth } = require('../middlewares');

router.get("/", auth, async (_req, res) => {
  res.json({
    message: "API Erick Whatsapp - 👋🌎🌍🌏",
  });
});

router.post("/send-message", auth, async (req, res) => {
  if (global.client && global.client !== "deploy") {
    if (!req.body.phone || !req.body.message) {
      return res.json({
        success: false,
        message: "phone & message required",
        data: null,
      });
    }
    if (req.body.image) {
      await global.client
        .sendImage(
          `${req.body.phone}@c.us`,
          req.body.image,
          new Date().getTime().toString(),
          req.body.message
        )
        .then((result) => {
          res.json({
            success: true,
            message: "success",
            data: result,
          });
        })
        .catch((error) => {
          res.json({
            success: false,
            message: error.message,
            data: error,
          });
        });
    } else {
      await global.client
      .sendText(`${req.body.phone}@c.us`, req.body.message)
      .then((result) => {
        res.json({
          success: true,
          message: "success",
          data: result,
        });
      })
      .catch((error) => {
        res.json({
          success: false,
          message: error.message,
          data: error,
        });
      });
    }
  } else {
    res.json({
      success: false,
      message:
        global.client && global.client === "deploy"
          ? "Device Restarting, please wait for a minute"
          : "Device Not Connected",
      data: null,
    });
  }
});

router.use("/image-scraper", imageScraperCtrl);

module.exports = router;
