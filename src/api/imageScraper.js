"use strict";
const express = require("express");
const { auth } = require("../middlewares");
const ImageScraper = require("images-scraper");

const router = express.Router();

router.get("/", (req, res) => {
  res.json({
    message: "API Erick Image Scraper - 👋🌎🌍🌏",
  });
});

router.get("/:image", auth, async (req, res) => {
  let imageSearch = req.params.image.split(",");
  const google = new ImageScraper({
    puppeteer: {
      headless: true,
      args: ['--no-sandbox', '--disable-setuid-sandbox']
    },
  });

  if (imageSearch && imageSearch.length > 0) {
    const results = await google.scrape(imageSearch.length === 1 ? imageSearch[0] : imageSearch, 10);
    res.json({
      success: true,
      message: "success",
      data: results,
    });
  } else {
    res.json({
      success: false,
      message: "image not found",
      data: null,
    });
  }
});

module.exports = router;
